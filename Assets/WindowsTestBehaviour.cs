﻿using UI;
using UI.Test;
using UnityEngine;
using UnityEngine.UI;

public sealed class WindowsTestBehaviour : MonoBehaviour {
    public Transform canvasRoot;

    [Space] public TextWindow textWindowPrefab;
    public Button textWindowButton;
    public TextWindow.Data textWindowData;

    [Space] public ImagesWindow imagesWindowPrefab;
    public Button imagesWindowButton;
    public string[] spritePaths;

    [Space] public PreviewWindow previewWindowPrefab;
    public Button previewWindowButton;
    public GameObject prefabToPreview;

    private TextWindow.Controller textController;
    private ImagesWindow.Controller imagesController;
    private PreviewWindow.Controller previewController;

    private void Awake() {
        WindowRegistry.SetRoot(canvasRoot);
        WindowRegistry.RegisterWindow(textWindowPrefab, 2);
        WindowRegistry.RegisterWindow(imagesWindowPrefab);
        WindowRegistry.RegisterWindow(previewWindowPrefab);

        textController = new TextWindow.Controller(textWindowData);
        imagesController = new ImagesWindow.Controller(spritePaths);
        previewController = new PreviewWindow.Controller(prefabToPreview);

        textWindowButton.onClick.AddListener(() => SwitchController(textController));
        imagesWindowButton.onClick.AddListener(() => SwitchController(imagesController));
        previewWindowButton.onClick.AddListener(() => SwitchController(previewController));
    }

    private void OnDestroy() {
        textWindowButton.onClick.RemoveAllListeners();
        imagesWindowButton.onClick.RemoveAllListeners();
        previewWindowButton.onClick.RemoveAllListeners();
        WindowRegistry.Cleanup();
    }

    private static void SwitchController(IWindowController controller) {
        if (controller.isOpen) {
            controller.HideWindow();
        } else {
            controller.ShowWindow();
        }
    }
}