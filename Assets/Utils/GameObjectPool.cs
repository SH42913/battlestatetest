﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Utils {
    public sealed class GameObjectPool : IDisposable {
        public bool isDisposed => poolRoot == null;

        private Transform poolRoot;
        private Dictionary<Type, InternalPool> typeToPool;

        public GameObjectPool(string poolName = "Pool") {
            poolRoot = new GameObject(poolName).transform;
            typeToPool = new Dictionary<Type, InternalPool>();
        }

        public bool HasPoolFor<T>() where T : PoolableBehaviour {
            return typeToPool.ContainsKey(typeof(T));
        }

        public void CreatePoolFor<T>(T prefab, int startCount = 1) where T : PoolableBehaviour {
            ThrowIfDisposed();

            var type = typeof(T);
            if (typeToPool.ContainsKey(type)) {
                throw new Exception($"Multiple pools for type {type.FullName} is not allowed!");
            }

            CreateNewPool(prefab, startCount);
        }

        public T GetInstanceOf<T>(Transform newParent) where T : PoolableBehaviour {
            ThrowIfDisposed();

            var instance = GetPoolFor(typeof(T)).GetInstance();
            instance.transform.SetParent(newParent);
            return (T) instance;
        }

        public void Dispose() {
            if (isDisposed) return;

            typeToPool.Clear();
            UnityEngine.Object.Destroy(poolRoot.gameObject);

            typeToPool = null;
            poolRoot = null;
        }

        private void CreateNewPool<T>(T prefab, int startCount = 1) where T : PoolableBehaviour {
            var pool = new InternalPool(this, prefab, startCount);
            pool.WarmPool(startCount);
            typeToPool.Add(typeof(T), pool);
        }

        private void Recycle(Type type, PoolableBehaviour instance) {
            ThrowIfDisposed();
            GetPoolFor(type).RecycleInstance(instance);
        }

        private InternalPool GetPoolFor(Type type) {
            return typeToPool.TryGetValue(type, out var pool) ? pool : throw new Exception($"Pool for type {type.FullName} was not created!");
        }

        private void ThrowIfDisposed() {
            if (isDisposed) {
                throw new ObjectDisposedException(nameof(GameObjectPool));
            }
        }

        private class InternalPool {
            private readonly Transform root;
            private readonly GameObjectPool main;
            private readonly PoolableBehaviour prefab;
            private readonly Stack<PoolableBehaviour> stack;

            public InternalPool(GameObjectPool mainPool, PoolableBehaviour original, int startStackSize) {
                main = mainPool;
                prefab = original;
                stack = new Stack<PoolableBehaviour>(startStackSize);

                root = new GameObject($"Pool for {prefab.name}").transform;
                root.gameObject.SetActive(false);
                root.SetParent(main.poolRoot);
            }

            public void WarmPool(int count) {
                for (var i = 0; i < count; i++) {
                    stack.Push(GetNewInstance());
                }
            }

            public void RecycleInstance(PoolableBehaviour instance) {
                instance.transform.SetParent(root);
                instance.gameObject.SetActive(false);
                stack.Push(instance);
            }

            public PoolableBehaviour GetInstance() => stack.Count > 0 ? stack.Pop() : GetNewInstance();

            private PoolableBehaviour GetNewInstance() {
                var instance = UnityEngine.Object.Instantiate(prefab, root);
                instance.gameObject.SetActive(false);
                PoolableBehaviour.SetPool(instance, main);
                return instance;
            }
        }

        public abstract class PoolableBehaviour : MonoBehaviour, IDisposable {
            private GameObjectPool pool;

            public void Recycle() {
                if (this == null) return;

                Dispose();
                if (pool == null || pool.isDisposed) {
                    Destroy(gameObject);
                } else {
                    pool.Recycle(GetType(), this);
                }
            }

            public virtual void Dispose() { }

            internal static void SetPool(PoolableBehaviour beh, GameObjectPool pool) {
                if (beh.pool == null) {
                    beh.pool = pool;
                }
            }
        }
    }
}