﻿using System;
using UnityEngine;
using Utils;

namespace UI {
    public sealed class WindowRegistry : IDisposable {
        private static Transform registryRoot;
        private static WindowRegistry cachedInstance;

        private static WindowRegistry instance {
            get {
                if (cachedInstance != null) return cachedInstance;
                if (registryRoot == null) throw new Exception($"You should call {nameof(SetRoot)} method before any actions!");

                cachedInstance = new WindowRegistry(new GameObjectPool($"{nameof(WindowRegistry)} Pool"), registryRoot);
                return cachedInstance;
            }
        }

        public static void SetRoot(Transform root) {
            if (cachedInstance != null) {
                Debug.LogWarning($"There is attempt to change Root for already created {nameof(WindowRegistry)}, " +
                                 $"Root will refresh only after {nameof(Cleanup)} call");
            }

            registryRoot = root;
        }

        public static void RegisterWindow<T>(T prefab, int poolWarmCount = 1) where T : GameObjectPool.PoolableBehaviour {
            instance.Register(prefab, poolWarmCount);
        }

        public static T GetWindowInstance<T>() where T : GameObjectPool.PoolableBehaviour {
            var window = instance.GetInstance<T>();
            window.transform.localScale = Vector3.one;
            if (window.transform is RectTransform rectTransform) {
                rectTransform.anchoredPosition = Vector2.zero;
            }

            return window;
        }

        public static void Cleanup() {
            cachedInstance?.Dispose();
            cachedInstance = null;
        }

        private readonly GameObjectPool pool;
        private readonly Transform root;

        private WindowRegistry(GameObjectPool pool, Transform root) {
            this.pool = pool;
            this.root = root;
        }

        private void Register<T>(T prefab, int poolWarmCount = 1) where T : GameObjectPool.PoolableBehaviour {
            if (pool.HasPoolFor<T>()) {
                Debug.LogError($"Window {typeof(T).FullName} already registered!");
            } else {
                pool.CreatePoolFor(prefab, poolWarmCount);
            }
        }

        private T GetInstance<T>() where T : GameObjectPool.PoolableBehaviour {
            return pool.GetInstanceOf<T>(root);
        }

        public void Dispose() {
            pool?.Dispose();
        }
    }
}