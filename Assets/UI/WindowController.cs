﻿using Utils;

namespace UI {
    public abstract class WindowController<TWindow> : IWindowController where TWindow : GameObjectPool.PoolableBehaviour {
        public bool isOpen { get; private set; }

        protected TWindow window;

        public void ShowWindow() {
            window = WindowRegistry.GetWindowInstance<TWindow>();
            PrepareWindow();
            window.gameObject.SetActive(true);
            isOpen = true;
        }

        protected abstract void PrepareWindow();

        public virtual void HideWindow() {
            window.Recycle();
            window = null;
            isOpen = false;
        }
    }

    public interface IWindowController {
        bool isOpen { get; }
        void ShowWindow();
        void HideWindow();
    }
}