﻿using UnityEngine;
using Utils;

namespace UI.Test {
    public sealed class PreviewWindow : GameObjectPool.PoolableBehaviour {
        [SerializeField] private Transform container = default;
        [SerializeField] private Camera usedCamera = default;
        [SerializeField] private LayerMask cameraLayer = default;

        public float itemRotationSpeed;

        private Transform itemInContainer;
        private bool hasItem;

        private void Awake() {
            usedCamera.cullingMask = cameraLayer;
        }

        private void Update() {
            if (hasItem) {
                itemInContainer.Rotate(Vector3.up, itemRotationSpeed * Time.unscaledTime);
            }
        }

        public override void Dispose() {
            if (itemInContainer == null) return;

            Destroy(itemInContainer.gameObject);
            itemInContainer = null;
            hasItem = false;
        }

        private void SetItem(GameObject prefab) {
            itemInContainer = Instantiate(prefab, container).transform;
            itemInContainer.gameObject.layer = ToLayer(cameraLayer);
            itemInContainer.gameObject.SetActive(true);
            hasItem = true;
        }

        private static int ToLayer(int bitmask) {
            var result = bitmask > 0 ? 0 : 31;
            while (bitmask > 1) {
                bitmask >>= 1;
                result++;
            }

            return result;
        }

        public sealed class Controller : WindowController<PreviewWindow> {
            public GameObject prefabToShow { private get; set; }

            public Controller(GameObject prefabToShow) {
                this.prefabToShow = prefabToShow;
            }

            protected override void PrepareWindow() => window.SetItem(prefabToShow);
        }
    }
}