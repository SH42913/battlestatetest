﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Test {
    public sealed class ImagesWindow : GameObjectPool.PoolableBehaviour {
        [SerializeField] private Transform container = default;
        [SerializeField] private Image imagePrefab = default;

        [SerializeField, Space] private Image progressBarImage = default;
        [SerializeField] private TMP_Text progressBarText = default;

        private readonly List<Image> activeImages = new List<Image>();
        private readonly Stack<Image> imagePool = new Stack<Image>();

        private void Awake() {
            imagePrefab.gameObject.SetActive(false);
        }

        public override void Dispose() {
            foreach (var image in activeImages) {
                image.sprite = null;
                image.gameObject.SetActive(false);
                imagePool.Push(image);
            }

            activeImages.Clear();
        }

        private void AddSprite(Sprite sprite) {
            var newImage = GetNewImage();
            newImage.sprite = sprite;
            newImage.gameObject.SetActive(true);
            activeImages.Add(newImage);
        }

        private void SetProgress(float progress) {
            if (progress < 0f || progress > 1f) {
                progressBarImage.gameObject.SetActive(false);
                return;
            }

            progressBarImage.gameObject.SetActive(true);
            progressBarImage.fillAmount = progress;
            progressBarText.text = progress.ToString("P1");
        }

        private Image GetNewImage() {
            var lastSibling = activeImages.Count > 0
                ? activeImages[activeImages.Count - 1].transform.GetSiblingIndex()
                : 1;

            if (imagePool.Count < 1) {
                return Instantiate(imagePrefab, container);
            }

            var image = imagePool.Pop();
            image.transform.SetSiblingIndex(lastSibling + 1);
            return image;
        }

        public sealed class Controller : WindowController<ImagesWindow> {
            public string[] spritePaths { private get; set; }

            private CancellationTokenSource cancellationSource;

            public Controller(string[] spritePaths) {
                this.spritePaths = spritePaths;
            }

            protected override void PrepareWindow() {
                cancellationSource = new CancellationTokenSource();
                LoadSpritesFromResources(spritePaths, cancellationSource.Token);
            }

            public override void HideWindow() {
                cancellationSource.Cancel();
                cancellationSource.Dispose();
                cancellationSource = null;
                base.HideWindow();
            }

            private async void LoadSpritesFromResources(string[] paths, CancellationToken cancellationToken) {
                window.SetProgress(0f);
                var currentIndex = 0;

                try {
                    foreach (var path in paths) {
                        if (cancellationToken.IsCancellationRequested) return;

                        var sprite = await LoadSprite(path, cancellationToken);
                        if (window == null) return;

                        var progress = (float) ++currentIndex / paths.Length;
                        window.SetProgress(progress);
                        window.AddSprite(sprite);
                    }
                } catch (OperationCanceledException) { }

                window.SetProgress(-1f);
            }

            private static async Task<Sprite> LoadSprite(string path, CancellationToken cancellationToken) {
                var delayTime = UnityEngine.Random.Range(500, 1500);
                await Task.Delay(delayTime, cancellationToken);
                return Resources.Load<Sprite>(path);
            }
        }
    }
}