﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Test {
    public sealed class TextWindow : GameObjectPool.PoolableBehaviour {
        [SerializeField] private TMP_Text headerText = default;
        [SerializeField] private TMP_Text mainText = default;

        private RectTransform rectTransform => (RectTransform) transform;

        private void SetHeader(string header) {
            headerText.text = header;
        }

        private void SetMainText(string text) {
            mainText.text = text;
        }

        private async Task PlayCloseAnimation(string byeText) {
            mainText.text = byeText;
            await Task.Delay(1000);
        }

#if UNITY_EDITOR
        private void OnValidate() {
            if (headerText == null) {
                headerText = FindTextWithName(nameof(headerText));
            }

            if (mainText == null) {
                mainText = FindTextWithName(nameof(mainText));
            }
        }

        private TMP_Text FindTextWithName(string textName) {
            return this
                .GetComponentsInChildren<TMP_Text>()
                .FirstOrDefault(x => x.name.StartsWith(textName, StringComparison.InvariantCultureIgnoreCase));
        }
#endif

        public sealed class Controller : WindowController<TextWindow> {
            public Data data { private get; set; }

            public Controller(Data data) {
                this.data = data;
            }

            protected override void PrepareWindow() {
                window.SetHeader(data.header);
                window.SetMainText(data.helloText);
                window.rectTransform.anchoredPosition = data.position;
            }

            public override async void HideWindow() {
                await window.PlayCloseAnimation(data.goodbyeText);
                base.HideWindow();
            }
        }

        [Serializable]
        public class Data {
            public string header;
            public string helloText;
            public string goodbyeText;
            public Vector2 position;
        }
    }
}